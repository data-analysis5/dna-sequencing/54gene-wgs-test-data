#!/bin/bash

set -euo pipefail
CMD="snakemake -p \
        --notemp \
	--use-conda \
	--conda-frontend mamba \
	--rerun-incomplete \
	--jobs 1"

echo "Command run:"
echo $CMD
eval $CMD 2>&1 | tee WGS_log.out 
