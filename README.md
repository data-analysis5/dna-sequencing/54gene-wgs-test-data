# 54gene WGS Germline Test Data
This repository contains small test datasets intended to be used to run with the [54gene-wgs-germline](https://gitlab.com/data-analysis5/dna-sequencing/54gene-wgs-germline) pipeline.

More specifically, this repository contains small subsets of chr 21 from the NA12878 standard genome. 
## Input data:
In this repository, you will find three subdirectories with the necessary files and inputs to run the 54gene-wgs-germline under all three run-modes: FastQC only, Full, and Joint Genotyping.


1. `full_mode`:
- contains 2 fastq.gz files for test read data
- contains a `config/` directory with a `manifest.txt` file containing paths to these fastq test files
- a `config.yaml` pre-configured to run full mode only
- a `intervals.tsv` file to parallelize variant calling and genotyping over 3 intervals 
- a `sex_linker.tsv` to test sex-check


2) `fastqc_only_mode`: 
- contains a `config/` directory with a `manifest.txt` file containing paths to the fastq test files 
- a `config.yaml` pre-configured to run fastqc mode only
- a `intervals.tsv` file to parallelize over 3 intervals 
- a `sex_linker.tsv` from above

3. `joint_geno_mode`
- contains a gVCF for the small subsetted region of chr21 for NA12878
- a `config.yaml` pre-configured to run joint genotyping mode only
- a `intervals.tsv` file to parallelize genotyping over 3 intervals 
- a `sex_linker.tsv` from above

### Resources

In `resources`, there are a set of resource files, originally derived from the Broad's resources at `s3://broad-references/hg38/v0/`.  The reference genome fasta has been subset to only chr21, and all indices have been updated.  The known_indels and dbsnp138 vcfs have also been subset to chr21, and all contigs in the headers have been removed except for chr21. 

In `resources/scattered_calling_intervals` you will also find the three interval regions used to parallelize the variant calling and joint genotyping steps of the pipeline, in the form of PICARD-style interval lists. 

To run tests using these resources, copy them into the `resources` directory in the pipeline you plan to run.  
## Intended Usage
The subdirectories are set up in such a way that you may copy in the `config/` directory into your current working pipeline directory for 54gene-wgs-germline and run the pipeline. 


Note: you may have to modify the root of the paths in the `manifest.txt` files prior to running.
  

## To run a test:

- Clone the wgs pipeline from `git@gitlab.com:data-analysis5/dna-sequencing/54gene-wgs-germline.git`
- Copy the `config/` directory of whichever run-mode you decide to use into your cloned wgs pipeline directory 
- Adjust paths in `config/manifest.txt` file as needed 
- Copy the resources from this repo into the pipeline `resources` directory
- Copy the run script from this repo and run the pipeline with that, in lieu of the usual `run.sh` script, to facilitate a local run (ie not on an HPC)

A log called `WGS_log.out` will be emitted containing all Snakemake trace.

Note: that scaled-up runs can be performed with the full NA12878 fastqs or subsets thereof as desired, as long as there is sufficient memory/compute.  This dataset, config, and resource folder are speciically designed to test the pipeline end-to-end without access to an HPC, eg on a personal machine, a single ec2 instance, or potentially integrated into CI/CD.
